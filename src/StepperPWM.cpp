#include "StepperPWM.h"
#include <string.h>

Stepper::Stepper(unsigned DIR_PIN, unsigned STEP_PIN, unsigned ENABLE_PIN, unsigned MAX_APPENDS = 10)
{
    _DIR_PIN = DIR_PIN;
    _STEP_PIN = STEP_PIN;
    _ENABLE_PIN = ENABLE_PIN;
    _MAX_APPENDS = MAX_APPENDS;
    pinMode(_DIR_PIN,OUTPUT);
    pinMode(_STEP_PIN,OUTPUT);
    pinMode(_ENABLE_PIN,OUTPUT);
    steps_lost = 0;
    steps_remaining_vector = new int[_MAX_APPENDS](0);
    steps_time_vector = new int[_MAX_APPENDS](0);
    done = true;
    digitalWrite(_STEP_PIN,LOW);
    digitalWrite(_ENABLE_PIN,DISABLE);

    switch(digitalPinToTimer(_STEP_PIN))
    {
        case TIMER0A:
            // Timer 0, channel A
            _MIN_STEP_TIME = 1024;
            break;

        case TIMER0B:
            // Timer 0, channel B
            _MIN_STEP_TIME = 1024;
            break;

        case TIMER1A:
            // Timer 1, channel A
            _MIN_STEP_TIME = 2040;
            break;

        case TIMER1B:
            // Timer 1, channel B
            _MIN_STEP_TIME = 2040;
            break;

        case TIMER2A:
            // Timer 2, channel A
            _MIN_STEP_TIME = 2040;
            break;

        case TIMER2B:
            // Timer 2, channel B
            _MIN_STEP_TIME = 2040;
            break;

        case NOT_ON_TIMER:
        default:
            Serial.println("STEP_PIN NOT VALID!");
            return 
    }
}

Stepper::~Stepper()
{
    delete[] steps_remaining_vector;
    delete[] steps_time_vector;
}

unsigned Stepper::getMinStepTime()
{
   return _MIN_STEP_TIME;
}

int Stepper::step(int steps, bool append = false)
{
    done = false;

    if (steps == 0)
    {
        return INVALID_STEP_NUMBER;
    }
    
    if (append && steps_remaining_vector[0] != 0)
    {
        if (appendSteps(steps) == IMPOSSIBLE_TO_APPEND)// || steps_remaining_vector[1] != 0)
        {
            Serial.println("ERROR: IMPOSSIBLE_TO_APPEND");
            return;
        }
    }
    else
    {
        bool direction = CLOCKWISE;
        flag = true;
        if (steps > 0){
            ++steps;  //for correctly counting the steps
            direction = CLOCKWISE;
        }
        if (steps < 0){
            --steps; //for correctly counting the steps
            direction = COUNTERCLOCKWISE;
        }

        steps_remaining = abs(steps);

        delay(10);
        digitalWrite(_ENABLE_PIN, ENABLE);
        digitalWrite(_DIR_PIN, direction);
        delay(10);

        steps_remaining_vector[0] = steps;
        Serial.println("Start stepping");

        switch(digitalPinToTimer(_STEP_PIN))
        {
            case TIMER0A:
                // Timer 0, channel A => port ~6
                sbi(TIMSK0, TOIE0); // enable overflow interrupt
                sbi(TCCR0A, COM0A1);// set pwm on port ~6
                OCR0A = DUTY_CYCLE; // set pwm duty cycle
                break;

            case TIMER0B:
                // Timer 0, channel B => port ~5
                sbi(TIMSK0, TOIE0); // enable overflow interrupt
                sbi(TCCR0A, COM0B1);// set pwm on port ~5
                OCR0B = DUTY_CYCLE; // set pwm duty cycle 
                break;

            case TIMER1A:
                // Timer 1, channel A => port ~9
                sbi(TIMSK1, TOIE1); // enable overflow interrupt
                TCNT1 = 0;
                break;

            case TIMER1B:
                // Timer 1, channel B => port ~10
                sbi(TIMSK1, TOIE1); // enable overflow interrupt
                sbi(TCCR1A, COM1B1);// set pwm on port ~10
                OCR1B = DUTY_CYCLE; // set pwm duty cycle
                break;

            case TIMER2A:
                // Timer 2, channel A => port ~11
                sbi(TIMSK2, TOIE2); // enable overflow interrupt
                sbi(TCCR2A, COM2A1);// set pwm on port ~11
                OCR2A = DUTY_CYCLE; // set pwm duty cycle
                break;

            case TIMER2B:
                // Timer 2, channel B => port ~3
                sbi(TIMSK2, TOIE2); // enable overflow interrupt
                sbi(TCCR2A, COM2B1);// set pwm on port
                OCR2B = DUTY_CYCLE; // set pwm duty cycle
                break;

            case TIMER3A:
                // Timer 3, channel A
                sbi(TIMSK3, TOIE3); // enable overflow interrupt
                sbi(TCCR3A, COM3A1);// set pwm on port
                OCR3A = DUTY_CYCLE; // set pwm duty cycle
                break;

            case TIMER3B:
                // Timer 3, channel B
                sbi(TIMSK3, TOIE3); // enable overflow interrupt
                sbi(TCCR3A, COM3B1);// set pwm on port
                OCR3B = DUTY_CYCLE; // set pwm duty cycle
                break;
                            
            case TIMER4A:
                // Timer 4, channel A
                sbi(TIMSK4, TOIE4); // enable overflow interrupt
                sbi(TCCR4A, COM4A1);// set pwm on port
                OCR4A = DUTY_CYCLE; // set pwm duty cycle
                break;

            case TIMER4B:
                // Timer 4, channel B
                sbi(TIMSK4, TOIE4); // enable overflow interrupt
                sbi(TCCR4A, COM4B1);// set pwm on port
                OCR4B = DUTY_CYCLE; // set pwm duty cycle
                break;
            
            case TIMER5A:
                // Timer 5, channel A
                sbi(TIMSK5, TOIE5); // enable overflow interrupt
                sbi(TCCR5A, COM5A1);// set pwm on port
                OCR5A = DUTY_CYCLE; // set pwm duty cycle
                break;

            case TIMER5B:
                // Timer 5, channel B
                sbi(TIMSK5, TOIE5); // enable overflow interrupt
                sbi(TCCR5A, COM5B1);// set pwm on port
                OCR5B = DUTY_CYCLE; // set pwm duty cycle
                break;

            case NOT_ON_TIMER:
            default:
                Serial.println("STEP_PIN NOT VALID!");
        }
        return steps_remaining;
    }
}

bool Stepper::ready()
{
    return done;
}

unsigned Stepper::stop()
{
    digitalWrite(_STEP_PIN,LOW);
    digitalWrite(_ENABLE_PIN,DISABLE);
    Serial.println("Stop stepping");
    
    done = true;

    switch(digitalPinToTimer(_STEP_PIN)) // remove overflow interruptions
    {
        case TIMER0A:
            // Timer 0, channel A
            cbi(TIMSK0, TOIE0); // remove overflow
            break;

        case TIMER0B:
            // Timer 0, channel B
            cbi(TIMSK0, TOIE0); // remove overflow
            break;

        case TIMER1A:
            // Timer 1, channel A
            cbi(TIMSK1, TOIE1); // remove overflow
            break;

        case TIMER1B:
            // Timer 1, channel B
            cbi(TIMSK1, TOIE1); // remove overflow
            break;

        case TIMER2A:
            // Timer 2, channel A
            cbi(TIMSK2, TOIE2); // remove overflow
            break;

        case TIMER2B:
            // Timer 2, channel B
            cbi(TIMSK2, TOIE2); // remove overflow
            break;

        case NOT_ON_TIMER:
        default:
            Serial.println("STEP_PIN NOT VALID!");
    }
    return getStepsRemaining();
}

unsigned Stepper::getStepsRemaining()
{
    unsigned steps = 0;
    for(int index=0; index<_MAX_APPENDS;index++)
    {
        steps += abs(steps_remaining_vector[index]);
    }
    return steps;
}

void Stepper::pulse()
{
    if(flag){
        sbi(TCCR1A, COM1A1);// set pwm on port ~9
        OCR1A = DUTY_CYCLE; // set pwm duty cycle
        flag=false;
    }
    --steps_remaining;
    if (steps_remaining == 0)
    {
        switch(digitalPinToTimer(_STEP_PIN))//pause interruptions
        {
            case TIMER0A:
                // Timer 0, channel A
                cbi(TIMSK0, TOIE0); // remove overflow
                break;

            case TIMER0B:
                // Timer 0, channel B
                cbi(TIMSK0, TOIE0); // remove overflow
                break;

            case TIMER1A:
                // Timer 1, channel A
                cbi(TIMSK1, TOIE1); // remove overflow
                break;

            case TIMER1B:
                // Timer 1, channel B
                cbi(TIMSK1, TOIE1); // remove overflow
                break;

            case TIMER2A:
                // Timer 2, channel A
                cbi(TIMSK2, TOIE2); // remove overflow
                break;

            case TIMER2B:
                // Timer 2, channel B
                cbi(TIMSK2, TOIE2); // remove overflow
                break;

            case NOT_ON_TIMER:
            default:
                Serial.println("STEP_PIN NOT VALID!");
        }
        TCNT1 = 0;
        digitalWrite(_STEP_PIN,LOW);
        steps_remaining_vector[0] = 0;
        if (steps_remaining_vector[1] != 0)
        {
            memcpy(steps_remaining_vector, steps_remaining_vector + 1,
                (_MAX_APPENDS - 1) * sizeof(*steps_remaining_vector));
            steps_remaining_vector[_MAX_APPENDS - 1] = 0;
            step(steps_remaining_vector[0], false);
        }
        else
        {
            stop();
        }
    }
}

int Stepper::appendSteps(int steps)
{
    unsigned index = 0;
    while ((steps_remaining_vector[index] != 0) && (index < _MAX_APPENDS))
    {
        ++index;
    }

    if (index < _MAX_APPENDS)
    {
        steps_remaining_vector[index] = steps;
        return getStepsRemaining();
    }
    else
    {
        Serial.println("ERROR: Impossible to append steps!");
        return IMPOSSIBLE_TO_APPEND;
    }
}
