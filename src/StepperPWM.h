#ifndef StepperPWM_h
#define StepperPWM_h

#include "Arduino.h"

#define CLOCKWISE 1;
#define COUNTERCLOCKWISE 0;

#define ENABLE  LOW
#define DISABLE HIGH

#define STEPPER_INTERRUPT_TIMER1(x) ISR(TIMER1_OVF_vect){ x.pulse(); }
#define STEPPER_INTERRUPT_TIMER2(x) ISR(TIMER2_OVF_vect){ x.pulse(); }
#define STEPPER_INTERRUPT_TIMER3(x) ISR(TIMER3_OVF_vect){ x.pulse(); }

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

//ERROR CODES
#define IMPOSSIBLE_TO_APPEND -1
#define INVALID_STEP_NUMBER  -2

class Stepper
{
    public:
        Stepper(unsigned DIR_PIN, unsigned STEP_PIN, unsigned ENABLE_PIN, unsigned MAX_APPENDS = 10); //time in microseconds
        ~Stepper();
        unsigned getMinStepTime(); //returns the minimal time for each step in microseconds
        int step(int steps, bool append = false);
        bool ready();
        unsigned stop();
        void pulse();

    private:
        unsigned _DIR_PIN;
        unsigned _STEP_PIN;
        unsigned _ENABLE_PIN;
        unsigned _MAX_APPENDS;
        unsigned steps_lost;
        int* steps_remaining_vector;
        int* steps_time_vector;
        int steps_remaining;
        unsigned _MIN_STEP_TIME; // in microseconds
        unsigned getStepsRemaining();
        int appendSteps(int steps);
        const int DUTY_CYCLE = 127;
        bool done;
        bool flag;
};

#endif