# Stepper-PWM Library

## Description

Simple stepper library to control stepper motors with Arduino using PWM-like output.

  

This library enables an Arduino board to control a stepper motor using a stepper motor driver such as A4988 or DRV8825. It uses the PWM signal to produce steps on the motor, making if turn as fast as possible. The library does not change the PWM frequency but uses interruptions to count the amount of steps sent to the motor. In order to do so, the library uses the internal Timers. The only Timer that is not used is Timer0 since it's used by many important functions of Arduino.h, e.g., *delay()*.

The library basically creates a new class called **Stepper**.

  

## Installation

To add a library manually, you need to download it as a ZIP file, expand it and put in the proper directory. The ZIP file contains all you need, including usage examples if the author has provided them. The library manager is designed to install this ZIP file automatically, but there are cases where you may want to perform the installation process manually and put the library in the libraries folder of your sketchbook by yourself.

  

- Download the library as a ZIP file.

- You can find or change the location of your sketchbook folder at *File > Preferences > Sketchbook* location.

- Go to the directory where you have downloaded the ZIP file of the library.

- Extract the ZIP file with all its folder structure in a temporary folder, then select the main folder, that should have the library name.

- Copy it in the “libraries” folder inside your sketchbook.

- Start the Arduino Software (IDE), go to *Sketch > Include Library*. Verify that the library you just added is available in the list.

  

For more information please consult [Installing Additional Arduino Libraries](https://www.arduino.cc/en/guide/libraries)
  
  

## Usage
First you'll have to crate an object of the type *Stepper* and then set the interruption for that object using STEPPER_INTERRUPT_TIMERn(_obejct_) where _n_ is the corresponding timer number of the step pin for that object (check the Pin To Timer table at the end). See example for better understanding.

### Stepper(unsigned  DIR_PIN, unsigned  STEP_PIN, unsigned  ENABLE_PIN, unsigned  MAX_APPENDS =  10)
Constructor used to initialize and setup the stepper motor.
* _unsigned DIR_PIN_ - indicates the Arduino pin connected to the _direction_ pin of the stepper driver
* _unsigned STEP_PIN_ - indicates the Arduino pin connected to the _step_ pin of the stepper driver
* _unsigned ENABLE_PIN_ - indicates the Arduino pin connected to the _enable_ pin of the stepper driver
* _unsigned MAX_APPENDS_ - Default: 10 - indicates the size of the append vector, how many appends can be done at once.

### unsigned  Stepper::getMinStepTime()
Returns the minimal time for each step in microseconds.

### int  Stepper::step(int  steps, bool  append =  false)
Method used to take steps. Returns the number of steps remaining or -1 case the step can't be done.
* _int steps_ - indicates the number of steps to be done, positive for clockwise and negative for counterclockwise.
* _bool append_ - Default: false - indicates if it is a step to be appended or to be done at once. _True_ indicates that the steps remaining will be done before the steps requested. _False_ indicates that all the remaining steps will be discarded and the requested steps will be done promptly.

### bool  Stepper::ready()
Method that return either if the motor has finished taking all the remaining steps.

### unsigned  Stepper::stop()
Method used to stop the motor. Returns the remaining steps.

__A4988:__

This library was made to be mainly used with the A4988 driver from Pololu ([datasheet](https://www.pololu.com/file/0J450/A4988.pdf)), here is the suggest connection:

![](images/A4988.jpg)

## Examples
### Control One Motor

```Arduino
#include <StepperPWM.h>

Stepper motor(8,9,7); //instantiate a Stepper object
STEPPER_INTERRUPT_TIMER1(motor) // using Timer 1 because the PWM on pin 9 is controlled by this Timer
int steps = 200; //200 steps is a full revolution for 1.8° step motors

void setup()
{
    Serial.begin(9600); // Serial used to debug
}

void loop()
{
    if(motor.ready())
    {
        motor.step(steps);
    }
    delay(5000); //substitute this delay with ther rest of your code
}
```

__Conections:__

Here is the suggested conection for this example with arduino:
![](images/ControlOneMotor.png)

## Pin To Timer
| Pin      | Arduino UNO  | Arduino MEGA 256 |
|:--------:|:------------:|:----------------:|
| 0        | NOT_ON_TIMER | NOT_ON_TIMER     |
| 1        | NOT_ON_TIMER | NOT_ON_TIMER     |
| 2        | NOT_ON_TIMER | TIMER3B          |
| 3        | TIMER2B      | TIMER3C          |
| 4        | NOT_ON_TIMER | TIMER0B          |
| 5        | TIMER0B      | TIMER3A          |
| 6        | TIMER0A      | TIMER4A          |
| 8        | NOT_ON_TIMER | TIMER4C          |
| 7        | NOT_ON_TIMER | TIMER4B          |
| 9        | TIMER1A      | TIMER2B          |
| 10       | TIMER1B      | TIMER2A          |
| 11       | TIMER2A      | TIMER1A          |
| 12       | NOT_ON_TIMER | TIMER1B          |
| 13       | NOT_ON_TIMER | TIMER0A          |
| 14 .. 43 |              | NOT_ON_TIMER     |
| 44       |              | TIMER5C          |
| 45       |              | TIMER5B          |
| 46       |              | TIMER5A          |

## License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).