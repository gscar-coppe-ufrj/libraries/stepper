#include <StepperPWM.h>

Stepper motor(8,9,7); //instantiate a Stepper object
STEPPER_INTERRUPT_TIMER1(motor) // using Timer 1 because the PWM on pin 9 is controlled by this Timer
int steps = 200; //200 steps is a full revolution for 1.8° step motors

void setup()
{
    Serial.begin(9600); // Serial used to debug
}

void loop()
{
    if(motor.ready())
    {
        motor.step(steps);
    }
    delay(5000); //substitute this delay with ther rest of your code
}
